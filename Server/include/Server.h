#ifndef SERVER_H
#define SERVER_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Server
{
    public:
        Server();
        ~Server();
        bool Run();
    private:
    //Network Initalsing
    sf::TcpListener listener;
    sf::SocketSelector selector;
    bool notRunning{false};

};

#endif // SERVER_H
