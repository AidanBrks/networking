#include "Server.h"

Server::Server()
{
    //ctor
}

Server::~Server()
{
    //dtor
}

bool Server::Run()
{
    std::cout << "Network & Multiplayer: " << "Server Started" << std::endl;
    //Create a Vector of Clients
    std::vector<sf::TcpSocket*> clients;

    listener.listen(25565);
    selector.add(listener);

    while(!notRunning)
    {
        if(selector.wait())
        {
            if(selector.isReady(listener))
            {
                //Waiting for a Client to Connect to the server
                sf::TcpSocket *socket = new sf::TcpSocket;
                listener.accept(*socket);
                sf::Packet packet;
                std::string id;
                if(socket->receive(packet) == sf::Socket::Done)
                    packet >> id;
                //Pushes back the client vector to add the new client.
                std::cout << id << " has connected to the server" << std::endl;
                clients.push_back(socket);
                selector.add(*socket);
            }
            else
            {
                for(int i = 0; i < clients.size(); i++)
                {
                    if(selector.isReady(*clients[i]))
                    {
                        sf::Packet packet, sendPacket;
                        if(clients[i]->receive(packet)== sf::Socket::Done)
                        {
                            std::string text;
                            packet >> text;
                            sendPacket << text;
                            for(int j = 0; j < clients.size(); j++)
                            {
                                // Checks if the Client is not the same one
                                // If not, It send the Packet
                                if(i != j)
                                {
                                    clients[j]->send(sendPacket);
                                }
                            }
                        }
                    }
                }
            }
        }

    }
        for(std::vector<sf::TcpSocket*>::iterator it = clients.begin(); it != clients.end(); it++)
            delete *it;


        return 0;
}
