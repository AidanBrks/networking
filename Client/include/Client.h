#ifndef CLIENT_H
#define CLIENT_H

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Client
{
    public:
        Client();
        ~Client();
        bool Run();
};

#endif // CLIENT_H
