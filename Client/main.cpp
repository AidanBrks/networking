/*
* Network & Multiplayer Server
* Client
*/
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <string>
#include <iostream>
#include <vector>

#include "Client.h"

using namespace std;

int main()
{
    // Run The Client
    Client client;
    client.Run();
}
