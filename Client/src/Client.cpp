#include "Client.h"

Client::Client()
{
    //ctor
}

Client::~Client()
{
    //dtor
}

bool Client::Run()
{
    std::cout << "Network and Multiplayer: Client Started" << std::endl;
    sf::IpAddress ip("127.0.0.1"); // Set the Ip Address to the Machine
    sf::TcpSocket socket;
    std::string id;
    std::string text = "";


    std::cout << "Enter Name: ";
    std::cin >> id; // Gather a Unique Identifier from the user

    socket.connect(ip,25565); // Connect to the IP Which Specified above and port given.

    sf::RenderWindow Window(sf::VideoMode(1920,1080,32), "Chat Client"); // Render the Window after getting ID & Connect
    std::vector<sf::Text> chat;

    // Send Unique Identifier
    sf::Packet packet;
    packet << id;
    socket.send(packet);
    socket.setBlocking(false);

    Window.setTitle(id); // Set it to the Window Name to the Unique ID

    sf::Font font;
    font.loadFromFile("Font.ttf"); // Load Font from Root Directory

    while(Window.isOpen())
    {
        sf::Event Event;
        while(Window.pollEvent(Event))
        {
            switch(Event.type)
            {
            case sf::Event::Closed:
                Window.close();
                break;
            case sf::Event::KeyPressed:
                if(Event.key.code == sf::Keyboard::Escape) // Close Window when Escape is Pressed
                    Window.close();
                else if(Event.key.code == sf::Keyboard::Return) // When Enter is Pressed Send with Unique Identifier.
                {
                    sf::Packet packet;
                    packet << id + ": " + text;
                    socket.send(packet);
                    sf::Text displayText(text, font, 20);
                    displayText.setColor(sf::Color::Red);
                    chat.push_back(displayText);
                    text= "";
                }
                break;
            case sf::Event::TextEntered:
                text += Event.text.unicode;
                break;
            }
        }
        //Recieve Chat and then render the text
        sf::Packet packet;
        socket.receive(packet);

        std::string temptext;
        if(packet >> temptext)
        {
            sf::Text displayText(temptext, font, 20);
            displayText.setColor(sf::Color::Blue);
            chat.push_back(displayText);
        }
        // Loop throught Chat Vector Offset Position
        int i = 0;
        for(i; i < chat.size(); i++)
        {
            chat[i].setPosition(0, i * 20);
            Window.draw(chat[i]);
        }
        sf::Text drawText(text, font, 20);
        drawText.setColor(sf::Color::Green);
        drawText.setPosition(0, i * 20);
        Window.draw(drawText);

        Window.display();
        Window.clear();

    }
    return 0;

}
